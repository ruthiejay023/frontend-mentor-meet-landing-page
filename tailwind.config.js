/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ['./public/**/*.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {},
    colors:{
      "light-green": "#4D96A9",
      "purple": "#855FB1",
      "deep-grey": "#28283D",
      "light-grey": "#87879D",
      "lighter-grey": "hsla(240, 10%, 57%, 0.25)",
      "sky-blue": "#8FE3F9",
      "light-purple": "#D9B8FF",
      "white": "#FAFAFA",
      "black": "#000000",
      "hover-first": "#71C0D4",
      "hover-second": "#B18BDD"
    },
    fontFamily: {
      RedHatDisplay: ['Red Hat Display', 'cursive']
    }
  },
  plugins: [],
}

